import json
from flask import Flask
app = Flask(__name__)


@app.route('/')
def index():
    return 'Index'


@app.route('/user/<username>')
def user(username):
    return f'Hello {username}'

@app.route('/add/<int:a>/<int:b>')
def add(a, b):
    result = a + b
    return f'{a} + {b} = {result}'


def db_get_user_list():
    user_list = [
        "issa",
        "camille",
        "justine",
        "andre",
    ]
    return user_list


@app.route('/users')
def users():
    user_list = db_get_user_list()
    user_list = json.dumps(user_list)
    response = {
        "user_list": user_list,
    }
    return f'{response}'
